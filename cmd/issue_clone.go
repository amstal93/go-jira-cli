package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/trivago/tgo/tcontainer"
	cliErrors "gitlab.com/pcanilho/go-jira-cli/cmd/errors"
	"gitlab.com/pcanilho/go-jira-cli/internal"
	"strings"
)

var cloneCmd = &cobra.Command{
	Use:   "clone",
	Short: "Clones a Jira ticket",
	RunE: func(cmd *cobra.Command, args []string) error {
		creationOpts := &internal.IssueCreationOptions{
			CustomFields: tcontainer.NewMarshalMap(),
		}

		if len(strings.TrimSpace(project)) != 0 {
			creationOpts.Project = project
		}
		if len(strings.TrimSpace(summary)) != 0 {
			creationOpts.Summary = summary
		}
		if len(strings.TrimSpace(issueType)) != 0 {
			creationOpts.IssueType = issueType
		}
		if len(strings.TrimSpace(description)) != 0 {
			creationOpts.Description = description
		}
		if len(strings.TrimSpace(epicLink)) != 0 {
			creationOpts.CustomFields.Set(internal.FieldEpiclink, epicLink)
		}

		// Validate, if provided, that the [Epic Link] value exists
		if len(strings.TrimSpace(epicLink)) != 0 {
			eIssue, err := jiraController.GetEpicLink(epicLink)
			if err != nil {
				return cliErrors.NewCliError(err)
			}
			creationOpts.CustomFields.Set(internal.FieldEpiclink, eIssue.Key)
		}

		for k, v := range customFields {
			creationOpts.CustomFields.Set(k, v)
		}

		issue, err := jiraController.CreateIssue(creationOpts)
		if err != nil {
			return cliErrors.NewCliError(err)
		}
		fmt.Println(issue.Key)
		return nil
	},
}

func init() {
	cloneCmd.Flags().StringVarP(&project, "project", "p", "", "the ticket project (optional)")
	cloneCmd.Flags().StringVarP(&issueType, "type", "t", "", "the ticket type (optional)")
	cloneCmd.Flags().StringVarP(&summary, "summary", "s", "", "the ticket summary (optional)")
	cloneCmd.Flags().StringVarP(&description, "description", "d", "", "the ticket description (optional)")
	cloneCmd.Flags().StringVarP(&description, "epic-link", "e", "", "the ticket epic (optional)")
	cloneCmd.Flags().StringToStringVar(&customFields, "custom-field", nil, "custom fields to attach to the ticket (optional)")
}

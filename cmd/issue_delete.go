package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	cliErrors "gitlab.com/pcanilho/go-jira-cli/cmd/errors"
	"gitlab.com/pcanilho/go-jira-cli/cmd/helpers"
)

var deleteCmd = &cobra.Command{
	Use:     "delete",
	Short:   "Deletes tickets/issues based on a provided ID or key",
	Example: fmt.Sprintf("%s %s delete key1 key2 ... keyN", name, issueCmd.Name()),
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			return fmt.Errorf("please specify the ticket key or ID to delete")
		}
		for _, issueID := range args {
			issue, err := jiraController.GetIssue(issueID, nil)
			if err != nil {
				return fmt.Errorf("unable to find a ticket with the key [%s]", issueID)
			}

			if !skipPrompts {
				cl := helpers.NewChangeLog()
				cl.AddChange("Issue deletion", issue.Key, "", false, true)
				fmt.Println(cl)
				confirmed, _ := helpers.PromptToContinue()
				if !confirmed {
					helpers.ExitWithAbort()
				}
			}

			if err = jiraController.DeleteIssue(issueID); err != nil {
				return cliErrors.NewCliError(err)
			}
			fmt.Printf("Deleted [%s]\n", issueID)
		}
		return nil
	},
}

module gitlab.com/pcanilho/go-jira-cli

go 1.14

require (
	github.com/andreyvit/diff v0.0.0-20170406064948-c7f18ee00883
	github.com/antonfisher/nested-logrus-formatter v1.3.1
	github.com/joonix/log v0.0.0-20200409080653-9c1d2ceb5f1d
	github.com/manifoldco/promptui v0.8.0
	github.com/pkg/errors v0.9.1
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
	github.com/trivago/tgo v1.0.7
	gitlab.com/pcanilho/go-jira v0.0.1
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/yaml.v2 v2.4.0
)

package internal

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/pcanilho/go-jira"
)

func (j *jiraController) GetEpicLink(epicID interface{}) (*jira.Issue, error) {
	// Get by key first
	epicByKey, err := j.getIssueFromIdentifier(epicID)
	if err == nil {
		return epicByKey, nil
	}

	// Search by "Epic Name" custom field
	epicByName, err := j.SearchIssues(fmt.Sprintf(`"Epic Name" ~ "%s"`, epicID), nil, 1)
	if err != nil || epicByName == nil || len(epicByName) == 0 {
		if err == nil {
			err = fmt.Errorf("not found")
		}
		return nil, errors.Wrapf(err, "unable to find a issue corresponding to the custom field [Epic Name] with value [%s]",
			epicID)
	}
	return &(epicByName[0]), nil
}

func (j *jiraController) SetEpicLink(epicID interface{}, issueID interface{}) (*jira.Issue, error) {
	if issueID == nil {
		return nil, fmt.Errorf("cannot update a nil instance of the [jira.Issue] object")
	}
	// Ascertain that destination issue exists
	issue, err := j.getIssueFromIdentifier(issueID)
	if err != nil {
		return nil, err
	}
	// Ascertain that the target [Epic Link/Name] exists
	epicIssue, err := j.GetEpicLink(epicID)
	if err != nil {
		return nil, err
	}
	issue.Fields.Unknowns.Set(FieldEpiclink, epicIssue.Key)
	updatedIssue, _, err := j.client.Issue.UpdateWithContext(j.ctx, issue)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to set the [Epic Link] with value [%s]", epicIssue.Fields.Summary)
	}
	return updatedIssue, nil
}
